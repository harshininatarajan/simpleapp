# Simple Calculator App

This is a simple calculator application built with Python. The repository includes the calculator app, a Jenkins pipeline configuration, and documentation for setting up a CI/CD pipeline using GitLab and Jenkins.

## Description

The Simple Calculator App allows users to perform basic arithmetic operations such as addition, subtraction, multiplication, and division. It also provides an option to exit the application.

## Features

- Addition
- Subtraction
- Multiplication
- Division
- Exit

## Prerequisites

- Python installed on your machine
- GitLab account
- Jenkins installed on a local machine or server

## Setup

### GitLab Repository

1. Create a new GitLab repository named "SimpleApp".
2. Initialize the repository with a README.md file.

### Jenkins Setup

1. Install Jenkins on your local machine or server by following the [official installation instructions](https://www.jenkins.io/doc/book/installing/).
2. Configure Jenkins to integrate with your GitLab repository:
   - Install the GitLab Plugin in Jenkins.
   - Navigate to Jenkins > Manage Jenkins > Configure System.
   - Under the GitLab section, add your GitLab server URL and credentials.

### Jenkins Pipeline

1. Clone or download this repository to your local machine.
2. Navigate to the directory containing the Jenkinsfile.
3. Open the Jenkinsfile in a text editor and update the GitLab repository URL.
4. Create a new Jenkins pipeline using the Jenkinsfile:
   - In Jenkins, navigate to New Item > Pipeline.
   - Enter a name for your pipeline and select "Pipeline" as the type.
   - Under the Pipeline section, select "Pipeline script from SCM".
   - Choose Git as the SCM, and enter the repository URL.
   - Save the pipeline configuration.

### GitLab CI Configuration

1. Create a .gitlab-ci.yml file in the root of your GitLab repository.
2. Configure GitLab CI to trigger the Jenkins pipeline on each commit to the repository:
   pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                git branch: 'main', url: 'https://gitlab.com/harshininatarajan/simpleapp.git'
            }
        }
        stage('Test') {
            steps {// Run unit tests (if any)
                bat 'python -m unittest'
            }
        }
    }
}
### Usage
- Clone or download the repository to your local machine.
- Navigate to the directory containing the calculator.py file.
- Open a terminal or command prompt.
- Run the following command to start the calculator app:
python calculator.py
- Follow the on-screen instructions to perform arithmetic operations.
- To exit the calculator app, select the "Exit" option from the menu.
### Testing
- Test the CI/CD pipeline by making a commit to the GitLab repository.
- Monitor the Jenkins dashboard to ensure that the pipeline executes successfully and deploys the application to the test environment.
